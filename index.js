const express = require('express')
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');

require('dotenv').config()

const { listMetrics, putMetricData, getMetricData, getMetricStat, putMultiMetricData, getMultiMetricData } = require('./cloudwatch');

const app = express()
//helmet helps you secure your Express apps by setting various HTTP headers.
app.use(helmet())
//enable cors
app.use(cors());
// use body-parser middleware
app.use(bodyParser.json({ limit: '10mb' }));

const port = process.env.PORT || 3000

// testing search approach

app.get('/list', async (req, res) => {
  try {
    const logs = await listMetrics(req.body)
    res.send({ success: true, message: 'got list', data: logs })
  } catch (error) {
    res.send({ success: false, ...error })
  }
})

app.get('/stat', async (req, res) => {
  try {
    const logs = await getMetricStat(req.body)
    res.send({ success: true, message: 'got stats', data: logs })
  } catch (error) {
    res.send({ success: false, ...error })
  }
})

app.get('/data', async (req, res) => {
  try {
    const logs = await getMetricData(req.body)
    res.send({ success: true, message: 'got data', data: logs })
  } catch (error) {
    res.send({ success: false, ...error })
  }
})

app.post('/', async (req, res) => {
  try {
    const logs = await putMetricData(req.body)
    res.send({ success: true, message: 'saved log', data: logs })
  } catch (error) {
    res.send({ success: false, ...error })
  }
})

//multi approach

app.get('/multi/data', async (req, res) => {
  try {
    const logs = await getMultiMetricData(req.body)
    res.send({ success: true, message: 'got metric data', data: logs })
  } catch (error) {
    res.send({ success: false, ...error })
  }
})

app.post('/multi', async (req, res) => {
  try {
    const logs = await putMultiMetricData(req.body)
    res.send({ success: true, message: 'saved multiple metric data', data: logs })
  } catch (error) {
    res.send({ success: false, ...error })
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
