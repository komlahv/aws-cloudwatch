var AWS = require('aws-sdk');

const ID = process.env.AWS_ID;
const SECRET = process.env.AWS_SECRET;

const cloudwatch = new AWS.CloudWatch({
  accessKeyId: ID,
  secretAccessKey: SECRET,
  apiVersion: '2014-03-28',
  region: 'eu-west-2',
  signatureVersion: 'v4',
});

// testing search approach

module.exports.listMetrics = (dimensions) => {

  const params = {
    Dimensions: dimensions,
    MetricName: 'JOB_DURATIONS',
    Namespace: 'jobs/duration',
  };

  return new Promise((resolve, reject) => {
    cloudwatch.listMetrics(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

module.exports.getMetricData = (dimensions) => {

  const now = new Date();

  const params = {
    "StartTime": new Date(now.setDate(now.getDate() - 30)),
    "EndTime": new Date(now.setDate(now.getDate() + 30)),
    "MetricDataQueries": [
      {
        "Id": "duration",
        "MetricStat": {
          "Metric": {
            "Namespace": "jobs/duration",
            "MetricName": "JOB_DURATIONS",
            "Dimensions": dimensions
          },
          "Period": 2628000, //month in seconds
          "Stat": "Sum",
          "Unit": "Seconds"
        },
        "ReturnData": true
      },
    ]
  };

  return new Promise((resolve, reject) => {
    cloudwatch.getMetricData(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

module.exports.getMetricStat = (dimensions) => {

  const now = new Date();

  const params = {
    "StartTime": new Date(now.setDate(now.getDate() - 30)),
    "EndTime": new Date(now.setDate(now.getDate() + 30)),
    "Namespace": "jobs/duration",
    "MetricName": "JOB_DURATIONS",
    Period: 2628000, //month in seconds
    Dimensions: dimensions,
    Statistics: [
      'Sum',
    ],
    Unit: 'Seconds'
  };

  return new Promise((resolve, reject) => {
    cloudwatch.getMetricStatistics(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

module.exports.putMetricData = ({ dimensions, value }) => {

  const params = {
    MetricData: [
      {
        MetricName: 'JOB_DURATIONS',
        Dimensions: dimensions,
        Unit: 'Seconds',
        Value: value
      },
    ],
    Namespace: 'jobs/duration'
  };

  return new Promise((resolve, reject) => {
    cloudwatch.putMetricData(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

// multi approach

module.exports.getMultiMetricData = (dimension) => {

  const now = new Date();

  const params = {
    "StartTime": new Date(now.setDate(now.getDate() - 30)),
    "EndTime": new Date(now.setDate(now.getDate() + 30)),
    "MetricDataQueries": [
      {
        "Id": "duration",
        "MetricStat": {
          "Metric": {
            "Namespace": "jobs/duration",
            "MetricName": "JOB_DURATIONS",
            "Dimensions": [dimension]
          },
          "Period": 2628000, //month in seconds
          "Stat": "Sum",
          "Unit": "Seconds"
        },
        "ReturnData": true //optional
      },
    ]
  };

  return new Promise((resolve, reject) => {
    cloudwatch.getMetricData(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

const putSingleMetricData = (dimension, value) => {
  const params = {
    MetricData: [
      {
        MetricName: 'JOB_DURATIONS',
        Dimensions: [
          dimension
        ],
        Unit: 'Seconds',
        Value: value
      },
    ],
    Namespace: 'jobs/duration'
  };

  return new Promise((resolve, reject) => {
    cloudwatch.putMetricData(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

module.exports.putMultiMetricData = ({ dimensions, value }) => {
  dimensions.map(async (dimension) => {
    await putSingleMetricData(dimension, value)
  })
};
